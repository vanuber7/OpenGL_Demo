#include "SpotLight.h"

///// DEFAULT CONSTRUCTOR /////
SpotLight::SpotLight()
{
	_type = "SpotLight";
	initialise();
}


///// DESTRUCTOR /////
SpotLight::~SpotLight()
{
	// Parent 'Light' deletes shader pointer
}


///// INITIALISE /////
bool SpotLight::initialise()
{
	if (_lightingShader->loadShaders("Shaders/LightingSpot.vert", "Shaders/LightingSpot.frag"))
	{
		return true;
	}
	return false;
}
