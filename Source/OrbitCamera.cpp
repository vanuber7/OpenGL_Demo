#include "OrbitCamera.h"
#include "glm/gtc/constants.hpp"


///// DEFAULT CONSTRUCTOR /////
OrbitCamera::OrbitCamera():
	_radius(0.0f)
{
}


///// DESTRUCTOR /////
OrbitCamera::~OrbitCamera()
{
}


///// ROTATE /////
void OrbitCamera::rotate(float yaw, float pitch)
{
	_yaw = glm::radians(yaw);
	_pitch = glm::radians(pitch);

	// Limit pitching up and down to the sky and floor
	_pitch = glm::clamp(_pitch, (-glm::pi<float>() / 2.0f) + 0.2f, (glm::pi<float>() / 2.0f) - 0.2f);

	updateCameraVectors();
}


///// SET LOOK AT /////
void OrbitCamera::setLookAt(const glm::vec3 & target)
{
	_targetPosition = target;
}


///// SET RADIUS /////
void OrbitCamera::setRadius(float radius)
{
	_radius = glm::clamp(radius, 2.0f, 20.0f);
}


///// UPDATE CAMERA VECTORS /////
void OrbitCamera::updateCameraVectors()
{
	// Spherical coordinates to Cartesian
	_position.x = _targetPosition.x + _radius * cosf(_pitch) * sinf(_yaw);
	_position.y = _targetPosition.y + _radius * sinf(_pitch);
	_position.z = _targetPosition.z + _radius * cosf(_pitch) * cosf(_yaw);
}
