#ifndef POINT_LIGHT_H
#define POINT_LIGHT_H

#include "Light.h"

///// POINT LIGHT CLASS /////
class PointLight : public Light
{
public:
	PointLight();
	~PointLight();

	// Inherited via Light
	virtual bool initialise() override;
};

#endif // POINT_LIGHT_H
