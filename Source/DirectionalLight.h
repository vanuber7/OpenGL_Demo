#ifndef DIRECTIONAL_LIGHT_H
#define DIRECTIONAL_LIGHT_H

#include "Light.h"

///// DIRECTIONAL LIGHT CLASS /////
class DirectionalLight : public Light
{
public:
	DirectionalLight();
	~DirectionalLight();

	// Inherited via Light
	virtual bool initialise() override;
};

#endif // DIRECTIONAL_LIGHT_H