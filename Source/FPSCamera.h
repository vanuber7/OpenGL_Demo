#ifndef _FPSCAMERA_H
#define _FPSCAMERA_H

#include "Camera.h"

///// FPS CAMERA CLASS (INHERITS FROM 'CAMERA') /////
class FPSCamera : public Camera
{
public:
	FPSCamera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f));
	virtual ~FPSCamera();

	// Inherited via Camera
	virtual void rotate(float yaw, float pitch) override;
	virtual void setPosition(const glm::vec3& position);
	virtual void move(const glm::vec3& offsetPosition);

protected:
	virtual void updateCameraVectors() override;
};

#endif // _FPSCAMERA_H