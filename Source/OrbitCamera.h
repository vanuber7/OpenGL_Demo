#ifndef ORBIT_CAMERA_H
#define ORBIT_CAMERA_H

#include "Camera.h"

///// ORBIT CAMERA CLASS (INHERITS FROM 'CAMERA') /////
class OrbitCamera : public Camera
{
public:
	OrbitCamera();
	virtual ~OrbitCamera();

	virtual void rotate(float yaw, float pitch) override;
	virtual void updateCameraVectors() override;
	void setLookAt(const glm::vec3& target);
	void setRadius(float radius);

protected:
	float _radius;
};

#endif // ORBIT_CAMERA_H

