#ifndef MESH_H
#define MESH_H

#include <vector>
#include <string>

#include "GLAD/glad.h"
#include "glm/glm.hpp"

///// VERTEX STRUCTURE /////
struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 textureCoord;
};


///// MESH CLASS /////
class Mesh
{
public:
	Mesh();
	~Mesh();

	bool loadOBJ(const std::string& fileName);
	void draw();

protected:
	void initialiseBuffers();

	bool _loaded;
	std::vector<Vertex> _vertices;
	GLuint _vbo;					// Vertex buffer object
	GLuint _vao;					// Vertex array object
};

#endif // MESH_H