#ifndef WINDOW_H
#define WINDOW_H

#define GLAD_GLFW
#include "GLAD\glad.h"
#include "GLFW\glfw3.h"

///// WINDOW CLASS /////
class Window
{
public:
	Window();
	~Window();

	bool createWindow();
	GLFWwindow* getHandle() const;

protected:
	GLFWwindow * _glfwWindow;
	const char* _GAME_TITLE;
	const GLboolean _FULL_SCREEN;
};

#endif // WINDOW_H