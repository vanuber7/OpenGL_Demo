#include "Camera.h"
#include "glm/gtc/matrix_transform.hpp"

///// DEFAULT CONSTRUCTOR /////
Camera::Camera() :
	_position(glm::vec3(0.0f, 0.0f, 0.0f)),
	_targetPosition(glm::vec3(0.0f, 0.0f, 0.0f)),
	_right(glm::vec3(0.0f, 0.0f, 0.0f)),
	_up(glm::vec3(0.0f, 1.0f, 0.0f)),
	//_look(glm::vec3(0.0f, 0.0f, 1.0f)),
	WORLD_UP(glm::vec3(0.0f, 1.0f, 0.0f)),
	_pitch(0.0f),
	_yaw(glm::pi<float>()),
	_fov(45.0f)
{
}


///// DESTRUCTOR /////
Camera::~Camera()
{
}


///// GET VIEW MATRIX /////
glm::mat4 Camera::getViewMatrix() const
{
	return glm::lookAt(_position, _targetPosition, _up);
}


///// GET RIGHT /////
const glm::vec3& Camera::getRight() const
{
	return _right;
}


///// GET UP /////
const glm::vec3& Camera::getUp() const
{
	return _up;
}


///// GET LOOK /////
const glm::vec3& Camera::getLook() const
{
	return _look;
}


///// GET POSITION /////
const glm::vec3 & Camera::getPosition() const
{
	return _position;
}
