#include <iostream>
#include "GLAD/glad.h"
#include "Scene.h"

Scene* scene = nullptr;

void setupCallbacks();
void changeBufferSizeCallback(GLFWwindow* window, const GLint width, const GLint height);
void processKeysCallback(GLFWwindow* window, GLint key, GLint scanCode, GLint action, GLint mode);
void processMouseScrollCallback(GLFWwindow* window, const double deltaX, const double deltaY);


///// MAIN /////
int main()
{
	scene = new Scene();

	if (scene->initialise())
	{
		setupCallbacks();
		bool quit = false;
		double lastTime = 0.0;

		// Cache the window pointer for the app loop
		GLFWwindow* appWindow = scene->getWindow();

		while (!glfwWindowShouldClose(appWindow))
		{
			double currentTime = glfwGetTime();
			double deltaTime = currentTime - lastTime;

			scene->update(deltaTime);
			scene->draw();

			lastTime = currentTime;
		}
	}
	
	delete scene;
	return EXIT_SUCCESS;
}


///// SETUP CALLBACKS /////
void setupCallbacks()
{
	glfwSetFramebufferSizeCallback(scene->getWindow(), changeBufferSizeCallback);
	glfwSetKeyCallback(scene->getWindow(), processKeysCallback);
	glfwSetScrollCallback(scene->getWindow(), processMouseScrollCallback);
}


///// CHANGE BUFFER SIZE CALLBACK /////
void changeBufferSizeCallback(GLFWwindow * window, const GLint width, const GLint height)
{
	scene->changeBufferSize(window, width, height);
}


///// PROCESS KEYS CALLBACK /////
void processKeysCallback(GLFWwindow * window, const GLint key, const GLint scanCode, const GLint action, const GLint mode)
{
	scene->processKeys(window, key, scanCode, action, mode);
}


///// PROCESS MOUSE SCROLL CALLBACK /////
void processMouseScrollCallback(GLFWwindow * window, const double deltaX, const double deltaY)
{
	scene->processMouseScroll(window, deltaX, deltaY);
}
