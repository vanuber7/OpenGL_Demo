#include <iostream>
#include "Model.h"

///// DEFAULT CONSTRUCTOR /////
Model::Model() :
	_mesh(nullptr),
	_texture2D(nullptr),
	_position(glm::vec3(0.0f)),
	_rotationAngle(0.0f),
	_scale(glm::vec3(1.0f))
{
}


///// DESTRUCTOR /////
Model::~Model()
{
	if (_mesh)
	{
		delete _mesh;
		_mesh = nullptr;
	}

	if (_texture2D)
	{
		delete _texture2D;
		_texture2D = nullptr;
	}
}


///// LOAD MODEL /////
bool Model::loadModel(const std::string& meshFileName, const std::string& textureFileName)
{
	_mesh = new Mesh();
	if (!_mesh->loadOBJ(meshFileName))
	{
		std::cout << "Failed to load model..." << std::endl;
		return false;
	}

	_texture2D = new Texture2D();
	if (!_texture2D->loadTexture(textureFileName))
	{
		std::cout << "Failed to load texture..." << std::endl;
		return false;
	}

	return true;
}


///// BIND TEXTURE /////
void Model::bindTexture(GLuint texUnit)
{
	_texture2D->bind(texUnit);
}


///// UNBIND TEXTURE /////
void Model::unbindTexture(GLuint texUnit)
{
	_texture2D->unbind(texUnit);
}


///// DRAW /////
void Model::draw()
{
	_mesh->draw();
}
