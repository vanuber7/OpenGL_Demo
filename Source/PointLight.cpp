#include "PointLight.h"

///// DEFAULT CONSTRUCTOR /////
PointLight::PointLight()
{
	_type = "PointLight";
	initialise();
}


///// DESTRUCTOR /////
PointLight::~PointLight()
{
	// Parent 'Light' deletes shader pointer
}


///// INITIALISE /////
bool PointLight::initialise()
{
	if (_lightingShader->loadShaders("Shaders/LightingPoint.vert", "Shaders/LightingPoint.frag"))
	{
		return true;
	}
	return false;
}