#include "Text.h"
#include "Constants.h"
#include "glm/gtc/matrix_transform.hpp"


///// DEFAULT CONSTRUCTOR /////
Text::Text():
	_shader(nullptr)
{
	_characters.clear();
}


///// DESTRUCTOR /////
Text::~Text()
{
	if (_shader != nullptr)
	{
		delete _shader;
		_shader = nullptr;
	}

	_characters.clear();
}


///// INITIALISE /////
bool Text::initialise()
{
	if (FT_Init_FreeType(&_freeType))
	{
		std::cerr << "Error: FreeType library failed to initialise" << std::endl;
		return false;
	}

	_shader	= new Shader();
	if (_shader->loadShaders("Shaders/TextShader.vert", "Shaders/TextShader.frag"))
	{
		_projection = glm::ortho(0.0f, (float)screenWidth, 0.0f, (float)screenHeight);
		_shader->use();
		_shader->setUniform("projection", _projection);
	}

	return true;
}


///// LOAD FONT /////
bool Text::loadFont(const std::string & fileName, const GLuint fontHeight)
{
	// Load the font as a 'face'
	if (FT_New_Face(_freeType, fileName.c_str(), 0, &_face))
	{
		std::cerr << "Error: Failed to load FreeType font" << std::endl;
		return false;
	}

	// Setting the width as zero makes the face calculate it based on the specified height.
	FT_Set_Pixel_Sizes(_face, 0, fontHeight);

	// Align all glyph textures to a single byte (8 bits)
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	for (GLubyte i = 0; i < 128; i++)
	{
		// Load all characters in the TTF file
		if (FT_Load_Char(_face, i, FT_LOAD_RENDER))
		{
			std::cerr << "Error: Failed to load glyph" << std::endl;
			continue;
		}

		// Generate a texture to load the character bitmap into
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			_face->glyph->bitmap.width,
			_face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			_face->glyph->bitmap.buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Insert this glyph info into a new 'Character'
		Character character;
		character._textureID = texture;
		character._size = glm::ivec2(_face->glyph->bitmap.width, _face->glyph->bitmap.rows);
		character._bearing = glm::ivec2(_face->glyph->bitmap_left, _face->glyph->bitmap_top);
		character._advance = _face->glyph->advance.x;

		// Store this 'Character' for later reference
		_characters.insert(std::pair<GLchar, Character>(i, character));
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	// Release FreeType resources
	FT_Done_Face(_face);
	FT_Done_FreeType(_freeType);

	// Enable alpha blending so the glyphs alpha values for the character pixels and the
	// transparent backgrounds are separated
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Reserve memory in the VBO for updating characters on the fly
	glGenVertexArrays(1, &_vao);
	glGenBuffers(1, &_vbo);
	glBindVertexArray(_vao);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);		// Each quad requires 6 verts x 4 floats of memory
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);		// 4 pieces of info(xPos, yPos, uCoord, vCoord)
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return true;
}


///// RENDER TEXT /////
void Text::renderText(const std::string & text, GLfloat x, GLfloat y, const GLfloat scale, const glm::vec3 colour)
{
	_shader->use();
	_shader->setUniformSampler("textSampler", 0);
	_shader->setUniform("textColour", colour);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(_vao);

	std::string::const_iterator iter;

	// Loop through the text string
	for (iter = text.begin(); iter != text.end(); iter++)
	{
		// Find this specific character in the map
		Character character = _characters[*iter];

		// Where should this glyph be drawn from?
		GLfloat xPos = x + character._bearing.x * scale;
		GLfloat yPos = y - (character._size.y - character._bearing.y) * scale;

		// Calculate the dimensions of the glyph texture
		GLfloat width = character._size.x * scale;
		GLfloat height = character._size.y * scale;

		GLfloat vertices[6][4] =
		{
			{	xPos,			yPos + height,	0.0f, 0.0f	},	// Bottom left
			{	xPos,			yPos,			0.0f, 1.0f	},	// Top left
			{	xPos + width,	yPos,			1.0f, 1.0f	},	// Top right

			{	xPos,			yPos + height,	0.0f, 0.0f	},	// Bottom left
			{	xPos + width,	yPos,			1.0f, 1.0f	},	// Top right
			{	xPos + width,	yPos + height,	1.0f, 0.0f	}	// Bottom right
		};

		glBindTexture(GL_TEXTURE_2D, character._textureID);
		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		// Bit shift to add the 'advance' value in pixels (not 64th of a pixel!)
		x += (character._advance >> 6) * (GLuint)scale;
	}

	// Detach from bindings
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}
