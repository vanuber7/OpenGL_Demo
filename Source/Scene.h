#ifndef SCENE_H
#define SCENE_H

#include "Window.h"
#include "Model.h"
#include "FPSCamera.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "SpotLight.h"
#include "Text.h"


///// SCENE CLASS /////
class Scene
{
public:
	Scene();
	~Scene();

	bool initialise();
	void update(double elapsedTime);
	void draw();
	void drawText();

	GLFWwindow* getWindow() const;
	void changeLightType();
	void setLight(Light* newLight);

	void changeBufferSize(GLFWwindow * window, const GLint width, const GLint height);
	void processKeys(GLFWwindow* window, const GLint key, const GLint scanCode, const GLint action, const GLint mode);
	void processMouseScroll(GLFWwindow * window, const double deltaX, const double deltaY);

protected:
	Window* _window;
	std::vector<Model*> _models;
	const int _NUM_MODELS;
	FPSCamera* _fpsCamera;
	Light* _currentLight;
	Text* _helpText;

	const float _MOUSE_SENSITIVITY;
	const float _ZOOM_SENSITIVITY;
	const float _MOVE_SPEED;

	GLboolean _showWireFrame;
	GLboolean _flashLightOn;
	GLboolean _isHelpActive;
};

#endif // SCENE_H

