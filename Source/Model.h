#ifndef MODEL_H
#define MODEL_H

#include "Mesh.h"
#include "Texture2D.h"

///// MODEL CLASS /////
class Model
{
public:
	Model();
	~Model();

	bool loadModel(const std::string& meshFileName, const std::string& textureFileName);
	void bindTexture(GLuint texUnit = 0);
	void unbindTexture(GLuint texUnit = 0);
	void draw();

	void setPosition(glm::vec3 position) { _position = position; };
	glm::vec3 getPosition() const { return _position; }


	void setRotation(GLfloat rotation) { _rotationAngle = rotation; };
	GLfloat getRotation() const { return _rotationAngle; }

	void setScale(glm::vec3 scale) { _scale = scale; }
	glm::vec3 getScale() const { return _scale; }

protected:
	Mesh* _mesh;
	Texture2D* _texture2D;
	glm::vec3 _position;
	GLfloat	_rotationAngle;
	glm::vec3 _scale;
};

#endif // MODEL_H