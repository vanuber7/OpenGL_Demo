#include <fstream>
#include <sstream>
#include "Shader.h"
#include "glm/gtc/type_ptr.hpp"

///// DEFAULT CONSTRUCTOR /////
Shader::Shader() :
	_handle(0)
{
	_uniformLocations.clear();
}


///// DESTRUCTOR /////
Shader::~Shader()
{
	glDeleteProgram(_handle);
	_uniformLocations.clear();
}


///// LOAD SHADERS /////
bool Shader::loadShaders(const GLchar* vsFileName, const GLchar* psFileName)
{
	// Read and convert shader files to strings
	std::string vsString = fileToString(vsFileName);
	std::string psString = fileToString(psFileName);
	const GLchar* vsSource = vsString.c_str();
	const GLchar* psSource = psString.c_str();

	// Create vertex and fragment shaders
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, &vsSource, nullptr);
	glShaderSource(fragmentShader, 1, &psSource, nullptr);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	checkForErrors(VERTEX, vertexShader);
	checkForErrors(FRAGMENT, fragmentShader);

	// Create, attach & link the shader program
	_handle = glCreateProgram();
	glAttachShader(_handle, vertexShader);
	glAttachShader(_handle, fragmentShader);
	glLinkProgram(_handle);

	checkForErrors(PROGRAM);

	// We don't need our shaders now so free them up
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	return true;
}


///// USE /////
void Shader::use()
{
	if (_handle > 0)
	{
		glUseProgram(_handle);
	}
}


///// FILE TO STRING /////
std::string Shader::fileToString(const std::string & fileName)
{
	std::stringstream stream;
	std::ifstream file;

	try
	{
		file.open(fileName, std::ios::in);

		// When successful let the stream consume the read buffer
		if (!file.fail())
		{
			stream << file.rdbuf();
		}

		file.close();
	}
	catch (std::exception& exception)
	{
		std::cerr << "Error: shader file could not be found!" << std::endl;
	}

	// All good, return the shader file contents
	return stream.str();
}


///// CHECK FOR ERRORS /////
void Shader::checkForErrors(ShaderType shaderType, GLuint shader)
{
	GLint result;

	if (shaderType == PROGRAM)
	{
		glGetProgramiv(_handle, GL_LINK_STATUS, &result);

		if (!result)
		{
			// Get the specific error length and put into a string
			GLint length = 0;
			glGetProgramiv(_handle, GL_INFO_LOG_LENGTH, &length);

			std::string errorLog(length, ' ');
			glGetProgramInfoLog(_handle, length, &length, &errorLog[0]);
			std::cerr << "Error: shader program failed to link. " << errorLog << std::endl;
		}
	}
	else
	// Vertex or fragment shader
	{
		glGetShaderiv(shader, GL_COMPILE_STATUS, &result);

		if (!result)
		{
			GLint length = 0;;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &result);
			std::string errorLog(length, ' ');
			glGetShaderInfoLog(_handle, length, &length, &errorLog[0]);
			std::cerr << "Error: shader failed to compile. " << errorLog << std::endl;
		}
	}
}


///// GET UNIFORM LOCATION /////
GLint Shader::getUniformLocation(const GLchar * name)
{
	// Find this uniform variable in the current program
	std::map<std::string, GLint>::iterator iter = _uniformLocations.find(name);

	// We haven't found this uniform already, so add it
	if (iter == _uniformLocations.end())
	{
		_uniformLocations[name] = glGetUniformLocation(_handle, name);
	}

	return _uniformLocations[name];
}


///// SET UNIFORM (INT) /////
void Shader::setUniform(const GLchar * name, const GLint value)
{
	GLint uniformID = getUniformLocation(name);
	glUniform1i(uniformID, value);
}


///// SET UNIFORM (FLOAT) /////
void Shader::setUniform(const GLchar * name, const GLfloat value)
{
	GLint uniformID = getUniformLocation(name);
	glUniform1f(uniformID, value);
}


///// SET UNIFORM (VEC2) /////
void Shader::setUniform(const GLchar * name, const glm::vec2 & value)
{
	GLint uniformID = getUniformLocation(name);
	glUniform2f(uniformID, value.x, value.y);
}


///// SET UNIFORM (VEC3) /////
void Shader::setUniform(const GLchar * name, const glm::vec3 & value)
{
	GLint uniformID = getUniformLocation(name);
	glUniform3f(uniformID, value.x, value.y, value.z);
}


///// SET UNIFORM (VEC4) /////
void Shader::setUniform(const GLchar * name, const glm::vec4 & value)
{
	GLint uniformID = getUniformLocation(name);
	glUniform4f(uniformID, value.x, value.y, value.z, value.w);
}


///// SET UNIFORM (4X4 MATRIX) /////
void Shader::setUniform(const GLchar * name, const glm::mat4 & value)
{
	GLint uniformID = getUniformLocation(name);
	glUniformMatrix4fv(uniformID, 1, GL_FALSE, glm::value_ptr(value));
}


///// SET UNIFORM SAMPLER (2D TEXTURE) /////
void Shader::setUniformSampler(const GLchar * name, const GLint & textureSlot)
{
	// Ensure this texture is activated before passing to the shader
	glActiveTexture(GL_TEXTURE0 + textureSlot);
	GLint uniformID = getUniformLocation(name);
	glUniform1i(uniformID, textureSlot);
}
