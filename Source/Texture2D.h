#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include <string>
#include "GLAD/glad.h"

class Texture2D
{
public:
	Texture2D();
	virtual ~Texture2D();

	bool loadTexture(const std::string& fileName, bool generateMipmaps = true);
	void bind(GLuint texUnit = 0);
	void unbind(GLuint texUnit = 0);

protected:
	GLuint _texture;
};

#endif // TEXTURE2D_H