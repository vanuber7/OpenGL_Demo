#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <map>
#include "GLAD/glad.h"
#include "glm/glm.hpp"


///// SHADER CLASS /////
class Shader
{
public:
	enum ShaderType
	{
		VERTEX,
		FRAGMENT,
		PROGRAM
	};

	Shader();
	~Shader();	

	bool loadShaders(const GLchar* vsFileName, const GLchar* psFileName);
	void use();

	void setUniform(const GLchar* name, const GLint value);
	void setUniform(const GLchar* name, const GLfloat value);
	void setUniform(const GLchar* name, const glm::vec2& value);
	void setUniform(const GLchar* name, const glm::vec3& value);
	void setUniform(const GLchar* name, const glm::vec4& value);
	void setUniform(const GLchar* name, const glm::mat4& value);
	void setUniformSampler(const GLchar* name, const GLint& textureSlot);

	GLuint getProgram() const { return _handle; }

protected:
	std::string fileToString(const std::string& fileName);
	void checkForErrors(ShaderType shaderType, GLuint shader = 0);
	GLint getUniformLocation(const GLchar* name);

	GLuint _handle;										// Shader program ID
	std::map<std::string, GLint> _uniformLocations;		// Shader uniform names/IDs
};

#endif // SHADER_H	