#ifndef CAMERA_H
#define CAMERA_H

#include "glm/glm.hpp"

///// CAMERA CLASS (ABSTRACT) /////
class Camera
{
public:
	virtual ~Camera();

	virtual void rotate(float yaw, float pitch) = 0;			// Degrees
	virtual void updateCameraVectors() = 0;
	virtual void setPosition(const glm::vec3& position) {};
	virtual void move(const glm::vec3& offsetPosition) {};

	glm::mat4 getViewMatrix() const;
	const glm::vec3& getRight() const;
	const glm::vec3& getUp() const;
	const glm::vec3& getLook() const;
	const glm::vec3& getPosition() const;

	float getFOV() const { return _fov; };		// Degrees
	void setFOV(float fov) { _fov = fov; };		// Degrees

protected:
	Camera();		// Can't be directly instantiated

	glm::vec3 _position;
	glm::vec3 _targetPosition;
	glm::vec3 _right;
	glm::vec3 _up;
	glm::vec3 _look;
	const glm::vec3 WORLD_UP;

	// Stored in radians
	float _yaw;
	float _pitch;

	// Stored in degrees
	float _fov;
};

#endif // CAMERA_H

