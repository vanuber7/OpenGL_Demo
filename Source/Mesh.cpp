#include <iostream>
#include <sstream>
#include <fstream>
#include "Mesh.h"

///// SPLIT /////
std::vector<std::string> split(std::string s, std::string token)
{
	std::vector<std::string> result;

	while (true)
	{
		int pos = s.find(token);
		// If we can't find the token
		if (pos == -1)
		{
			// Store the last result. We are done
			result.push_back(s);
			break;
		}

		// Chop up the string based on the token
		result.push_back(s.substr(0, pos));
		s = s.substr(pos + 1, s.size() - pos - 1);
	}

	return result;
}


///// DEFAULT CONSTRUCTOR /////
Mesh::Mesh():
	_loaded(false)
{
}


///// DESTRUCTOR /////
Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &_vbo);
	glDeleteBuffers(1, &_vao);
	_vertices.clear();
}


///// LOAD OBJ /////
bool Mesh::loadOBJ(const std::string & fileName)
{
	std::vector<unsigned int> vertexIndices, normalIndices, uvIndices;
	std::vector<glm::vec3> tempVertices;
	std::vector<glm::vec3> tempNormals;
	std::vector<glm::vec2> tempUVs;

	// Are we reading a valid .obj file?
	if (fileName.find(".obj") != std::string::npos)
	{
		// Check we can open the file
		std::ifstream file(fileName, std::ios::in);
		if (!file)
		{
			std::cerr << "Failed to open file... " << fileName << std::endl;
			return false;
		}

		std::cout << "Loading OBJ file... " << fileName << std::endl;


		std::string lineBuffer;
		// Read lines until the end of the file
		while (std::getline(file, lineBuffer))
		{
			// Get the first string from the line which is the 'command' type
			std::stringstream newLine(lineBuffer);
			std::string command;
			newLine >> command;

			// Check for a 'vertex position' definition at the start of this line
			if (command == "v")
			{
				// Put the stream from this line into a new vertex
				glm::vec3 vertex;
				int dimension = 0;
				while (dimension < 3 && newLine >> vertex[dimension])
				{
					dimension++;
				}
				tempVertices.push_back(vertex);
			}
			// Same for a 'vertex texture coordinate'
			else if(command == "vt")
			{
				glm::vec2 uvCoord;
				int dimension = 0;
				while (dimension < 2 && newLine >> uvCoord[dimension])
				{
					dimension++;
				}
				tempUVs.push_back(uvCoord);
			}
			// Same again for vertex normals
			else if (command == "vn")
			{
				glm::vec3 normal;
				int dimension = 0;
				while (dimension < 3 && newLine >> normal[dimension])
				{
					dimension++;
				}
				// Ensure the 'normal' is of unit length
				normal = glm::normalize(normal);
				tempNormals.push_back(normal);
			}
			// Split out indices for the face
			else if (command == "f")
			{
				std::string faceData;
				int vertexIndex;
				int uvIndex;
				int normalIndex;

				// Split the face data up into it's components (V/VT/VN)
				while (newLine >> faceData)
				{
					std::vector<std::string> data = split(faceData, "/");

					// Read the formatted data and place into the indices
					// The first element is a vertex
					if (data[0].size() > 0)
					{					
						sscanf_s(data[0].c_str(), "%d", &vertexIndex);
						vertexIndices.push_back(vertexIndex);
					}

					// Face has a texture coordinate index
					if (data.size() >= 1)
					{
						if (data[1].size() > 0)
						{						
							sscanf_s(data[1].c_str(), "%d", &uvIndex);
							uvIndices.push_back(uvIndex);
						}
					}

					// Face has a normal index
					if (data.size() >= 2)
					{
						if (data[2].size() > 0)
						{
							sscanf_s(data[2].c_str(), "%d", &normalIndex);
							normalIndices.push_back(normalIndex);
						}
					}
				}
			}
		}

		file.close();

		// Put the info from indices into our vertices
		for (unsigned int i = 0; i < vertexIndices.size(); i++)
		{
			Vertex meshVertex;

			// Ensure we have read valid vertex information
			// Subtract 1 for 1 based indices in file
			if (tempVertices.size() > 0)
			{
				glm::vec3 vertex = tempVertices[vertexIndices[i] - 1];
				meshVertex.position = vertex;
			}
			if (tempUVs.size() > 0)
			{
				glm::vec2 uv = tempUVs[uvIndices[i] - 1];
				meshVertex.textureCoord = uv;
			}
			if (tempNormals.size() > 0)
			{
				glm::vec3 normal = tempNormals[normalIndices[i] - 1];
				meshVertex.normal = normal;
			}
			_vertices.push_back(meshVertex);
		}

		initialiseBuffers();
		return (_loaded = true);
	}

	// We only get here if there was an error with the file
	return false;
}


///// DRAW /////
void Mesh::draw()
{
	if (!_loaded)
	{
		return;
	}

	// Draw all the verts in the mesh
	glBindVertexArray(_vao);
	glDrawArrays(GL_TRIANGLES, 0, _vertices.size());
	glBindVertexArray(0);
}


///// INITIALISE BUFFERS /////
void Mesh::initialiseBuffers()
{
	// Vertex buffer object
	glGenBuffers(1, &_vbo);

	// Bind position buffer
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex), &_vertices[0], GL_STATIC_DRAW);

	// Vertex array object
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	// Position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);
	glEnableVertexAttribArray(0);

	// Normals
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	// Texture coordinates
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	// Don't let anything else change this VAO
	glBindVertexArray(0);
}