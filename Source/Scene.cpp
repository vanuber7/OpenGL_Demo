#include <iostream>
#include "glm/gtc/matrix_transform.hpp"

#include "Scene.h"
#include "Constants.h"

///// DEFAULT CONSTRUCTOR /////
Scene::Scene() :
	_window(nullptr),
	_NUM_MODELS(8),
	_fpsCamera(nullptr),
	_currentLight(nullptr),
	_helpText(nullptr),
	_MOUSE_SENSITIVITY(0.1f),
	_ZOOM_SENSITIVITY(-3.0f),
	_MOVE_SPEED(10.0f),
	_showWireFrame(GL_FALSE),
	_flashLightOn(GL_TRUE),
	_isHelpActive(GL_FALSE)
{
	_models.clear();
}


///// DESTRUCTOR  /////
Scene::~Scene()
{
	// Remove all assets
	if (_fpsCamera != nullptr)
	{
		delete _fpsCamera;
		_fpsCamera = nullptr;
	}

	if (_currentLight != nullptr)
	{
		delete _currentLight;
		_currentLight = nullptr;
	}

	if (_helpText != nullptr)
	{
		delete _helpText;
		_helpText = nullptr;
	}

	for (int i = 0; i < _NUM_MODELS; i++)
	{
		if (_models[i] != nullptr)
		{
			delete _models[i];
			_models[i] = nullptr;
		}
	}
	_models.clear();

	// GLFW should be terminated last
	if (_window)
	{
		delete _window;
		_window = nullptr;
	}
}


///// INITIALISE  /////
bool Scene::initialise()
{
	_window = new Window();
	if (!_window->createWindow())
	{
		std::cout << "Failed to create window" << std::endl;
		return false;
	}

	// Initialise model pointers
	for (int i = 0; i < _NUM_MODELS; i++)
	{
		Model* model = new Model();
		_models.push_back(model);
	}

	// Load model meshes, texture with position/scale
	_models[0]->loadModel("Models/Crate.obj", "Textures/Crate.jpg");
	_models[0]->setPosition(glm::vec3(-2.5f, 1.0f, -3.0f));
	_models[1]->loadModel("Models/WoodCrate.obj", "Textures/WoodCrate.jpg");
	_models[1]->setPosition(glm::vec3(2.5f, 1.0f, -3.0f));
	_models[2]->loadModel("Models/Robot.obj", "Textures/Robot.jpg");
	_models[2]->setPosition(glm::vec3(-5.0f, 0.0f, -2.0f));
	_models[2]->setRotation(30.0f);
	_models[3]->loadModel("Models/Robot.obj", "Textures/Robot.jpg");
	_models[3]->setPosition(glm::vec3(5.0f, 0.0f, -2.0f));
	_models[3]->setRotation(-30.0f);
	_models[4]->loadModel("Models/Floor.obj", "Textures/TileFloor.jpg");
	_models[4]->setPosition(glm::vec3(0.0f));
	_models[4]->setScale(glm::vec3(5.0f, 1.0f, 5.0f));
	_models[5]->loadModel("Models/BowlingPin.obj", "Textures/AMF.tga");
	_models[5]->setPosition(glm::vec3(2.0f, 0.0f, 0.0f));
	_models[5]->setScale(glm::vec3(0.1f));
	_models[6]->loadModel("Models/Bunny.obj", "Textures/Bunny.jpg");
	_models[6]->setPosition(glm::vec3(-2.0f, 0.0f, 0.0f));
	_models[6]->setScale(glm::vec3(0.7f));
	_models[7]->loadModel("Models/LampPost.obj", "Textures/LampPost.png");
	_models[7]->setPosition(glm::vec3(0.0f, 0.0f, -1.5f));

	// Move the camera back to view the scene
	_fpsCamera = new FPSCamera(glm::vec3(0.0f, 2.5f, 10.0f));

	setLight(new DirectionalLight());

	_helpText = new Text();
	if (_helpText->initialise())
	{
		_helpText->loadFont("Fonts/ERASMD.ttf", 24);
	}

	return true;
}


///// UPDATE  /////
void Scene::update(double elapsedTime)
{
	glfwPollEvents();

	// FPSCamera rotation
	double mouseX, mouseY;

	glfwGetCursorPos(getWindow(), &mouseX, &mouseY);

	// Rotate the camera based on mouse movement relative to the center of the screen
	_fpsCamera->rotate((float)(screenWidth / 2.0 - mouseX) * _MOUSE_SENSITIVITY, (float)(screenHeight / 2.0 - mouseY) * _MOUSE_SENSITIVITY);

	// Reset cursor position to center for next check
	glfwSetCursorPos(getWindow(), screenWidth / 2.0, screenHeight / 2.0);


	// FPSCamera movement
	// Forwards/backwards
	if (glfwGetKey(getWindow(), GLFW_KEY_W) == GLFW_PRESS)
	{
		_fpsCamera->move(_fpsCamera->getLook() * _MOVE_SPEED * (float)elapsedTime);
	}
	else if (glfwGetKey(getWindow(), GLFW_KEY_S) == GLFW_PRESS)
	{
		_fpsCamera->move(-_fpsCamera->getLook() * _MOVE_SPEED * (float)elapsedTime);
	}

	// Strafe left/right
	if (glfwGetKey(getWindow(), GLFW_KEY_A) == GLFW_PRESS)
	{
		_fpsCamera->move(-_fpsCamera->getRight() * _MOVE_SPEED * (float)elapsedTime);
	}
	else if (glfwGetKey(getWindow(), GLFW_KEY_D) == GLFW_PRESS)
	{
		_fpsCamera->move(_fpsCamera->getRight() * _MOVE_SPEED * (float)elapsedTime);
	}

	// Up/down
	if (glfwGetKey(getWindow(), GLFW_KEY_Q) == GLFW_PRESS)
	{
		_fpsCamera->move(_fpsCamera->getUp() * _MOVE_SPEED * (float)elapsedTime);
	}
	else if (glfwGetKey(getWindow(), GLFW_KEY_E) == GLFW_PRESS)
	{
		_fpsCamera->move(-_fpsCamera->getUp() * _MOVE_SPEED * (float)elapsedTime);
	}
}


///// DRAW  /////
void Scene::draw()
{
	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Declare matrix transformations
	glm::mat4 model, view, projection;

	// View matrix
	view = _fpsCamera->getViewMatrix();

	// Projection matrix (perspective)
	float fov = glm::radians(_fpsCamera->getFOV());					// MUST BE RADIANS!!
	float aspectRatio = (float)screenWidth / (float)screenHeight;
	float nearClip = 0.1f;
	float farClip = 100.0f;
	projection = glm::perspective(fov, aspectRatio, nearClip, farClip);


	// We need to use our shader program before we set uniforms and draw
	_currentLight->useShader();

	// Where is the camera's current position?
	glm::vec3 eyePosition = _fpsCamera->getPosition();

	// Shader vertex transformations
	_currentLight->setMatrix4x4("view", view);
	_currentLight->setMatrix4x4("projection", projection);
	_currentLight->setVector3("eyePosition", eyePosition);

	// Apply light attributes to the shader
	glm::vec3 lightColour(1.0f);
	_currentLight->setVector3("light.ambient", glm::vec3(0.1f));
	_currentLight->setVector3("light.diffuse", lightColour);
	_currentLight->setVector3("light.specular", glm::vec3(0.8f));

	// Material attributes
	_currentLight->setVector3("material.ambient", glm::vec3(0.2f));
	_currentLight->setSampler("material.diffuseMap", 0);
	_currentLight->setVector3("material.specular", glm::vec3(0.8f));
	_currentLight->setFloat("material.shininess", 32.0f);


	// Light direction
	if (_currentLight->getType() == "DirectionalLight")
	{
		glm::vec3 lightDirection = glm::vec3(0.3f, -0.7f, -0.5f);
		_currentLight->setVector3("light.direction", lightDirection);
	}
	else if (_currentLight->getType() == "SpotLight")
	{
		glm::vec3 lightDirection = _fpsCamera->getLook();
		_currentLight->setVector3("light.direction", lightDirection);
	}


	// Light position
	if (_currentLight->getType() == "PointLight")
	{
		// Position inside the lamp post
		glm::vec3 lightPosition = glm::vec3(0.0f, 3.73f, -1.5f);
		_currentLight->setVector3("light.position", lightPosition);

		// Yellow light lamp post
		glm::vec3 lightColour = glm::vec3(2.0f, 2.0f, 0.5f);
		_currentLight->setVector3("light.diffuse", lightColour);
	}
	else if (_currentLight->getType() == "SpotLight")
	{
		// Hold just below player's eye line
		glm::vec3 lightPosition = _fpsCamera->getPosition();
		lightPosition.y -= 0.5f;
		_currentLight->setVector3("light.position", lightPosition);

		glm::vec3 lightColour = glm::vec3(2.0f, 2.0f, 2.0f);
		_currentLight->setVector3("light.diffuse", lightColour);
	}
	

	// Attenuation
	if (_currentLight->getType() == "PointLight" || _currentLight->getType() == "SpotLight")
	{
		_currentLight->setFloat("light.constant", 1.0f);
		_currentLight->setFloat("light.linear", 0.07f);
		_currentLight->setFloat("light.exponent", 0.017f);
	}
	

	// Cone angles
	if (_currentLight->getType() == "SpotLight")
	{
		_currentLight->setFloat("light.cosInnerCone", glm::cos(glm::radians(10.0f)));
		_currentLight->setFloat("light.cosOuterCone", glm::cos(glm::radians(15.0f)));
		_currentLight->setInt("light.on", _flashLightOn);
	}

	// Draw models in wire frame mode if active
	if (_showWireFrame)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	
	// Draw models
	for (int i = 0; i < _NUM_MODELS; i++)
	{
		// Build the matrices from identity matrices (glm::mat4())
		// All rotations are about the Y axis
		// This transformation results in the model matrix for this specific model
		model = glm::translate(glm::mat4(), _models[i]->getPosition()) * 
				glm::rotate(glm::mat4(), glm::radians(_models[i]->getRotation()), glm::vec3(0.0f, 1.0f, 0.0f)) * 
				glm::scale(glm::mat4(), _models[i]->getScale());

		_currentLight->setMatrix4x4("model", model);

		_models[i]->bindTexture(0);
		_models[i]->draw();
		_models[i]->unbindTexture();
	}

	drawText();

	glfwSwapBuffers(_window->getHandle());
}


///// DRAW TEXT  /////
void Scene::drawText()
{
	// Always show the text filled in
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (_isHelpActive)
	{
		_helpText->renderText("Forward/back (W/S)", 10.0f, screenHeight - 30.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
		_helpText->renderText("Left/right (A/D)", 10.0f, screenHeight - 60.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
		_helpText->renderText("Up/Down (Q/E)", 10.0f, screenHeight - 90.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
		_helpText->renderText("Look (Mouse)", 10.0f, screenHeight - 120.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
		_helpText->renderText("Zoom (Mouse scroll)", 10.0f, screenHeight - 150.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
		_helpText->renderText("Light type (L)", 10.0f, screenHeight - 180.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
		_helpText->renderText("Toggle flashlight (F)", 10.0f, screenHeight - 210.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
		_helpText->renderText("Wire frame mode (Space)", 10.0f, screenHeight - 240.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
		_helpText->renderText("Quit (Esc)", 10.0f, screenHeight - 270.0f, 1.0f, glm::vec3(1.0f, 1.0f, 1.0f));
	}

	_helpText->renderText("Help (F1)", 10.0f, 10.0f, 1.0f, glm::vec3(1.0f, 1.0f, 0.0f));
}


///// GET WINDOW  /////
GLFWwindow* Scene::getWindow() const
{
	if (_window != nullptr)
	{
		return _window->getHandle();
	}
	return nullptr;
}


///// CHANGE LIGHT TYPE  /////
void Scene::changeLightType()
{
	// Cycle through the different light types
	if (_currentLight->getType() == "DirectionalLight")
	{
		setLight(new PointLight());
	}
	else if (_currentLight->getType() == "PointLight")
	{
		setLight(new SpotLight());
	}
	else
	{
		setLight(new DirectionalLight());
	}
}


///// SET LIGHT  /////
void Scene::setLight(Light* newLight)
{
	if (_currentLight != nullptr)
	{
		delete _currentLight;
		_currentLight = nullptr;
	}

	_currentLight = newLight;
}


///// CHANGE BUFFER SIZE /////
void Scene::changeBufferSize(GLFWwindow * window, const GLint width, const GLint height)
{
	screenWidth = width;
	screenHeight = height;
	glViewport(0, 0, screenWidth, screenHeight);
}


///// PROCESS KEYS /////
void Scene::processKeys(GLFWwindow* window, const GLint key, const GLint scanCode, const GLint action, const GLint mode)
{
	// End the app (Escape)
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(getWindow(), GL_TRUE);
		return;
	}

	// Toggle wire frame mode on and off (SPACE)
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
	{
		_showWireFrame = !_showWireFrame;
	}

	// Toggle flashlight(spot light) on/off (F)
	if (key == GLFW_KEY_F && action == GLFW_PRESS)
	{
		if (_currentLight->getType() == "SpotLight")
		{
			_flashLightOn = !_flashLightOn;
		}
	}

	// Toggle light type (L)
	if (key == GLFW_KEY_L && action == GLFW_PRESS)
	{
		changeLightType();
	}

	// Toggle help/controls (F1)
	if (key == GLFW_KEY_F1 && action == GLFW_PRESS)
	{
		_isHelpActive = !_isHelpActive;
	}
}


///// PROCESS MOUSE SCROLL /////
void Scene::processMouseScroll(GLFWwindow * window, const double deltaX, const double deltaY)
{
	// Change the FOV with the mouse scroll to give the illusion of zooming in/out
	float fov = _fpsCamera->getFOV() + ((float)deltaY * _ZOOM_SENSITIVITY);

	fov = glm::clamp(fov, 10.0f, 120.0f);
	_fpsCamera->setFOV(fov);
}
