#ifndef TEXT_H
#define TEXT_H

#include <ft2build.h>
#include FT_FREETYPE_H
#include "Shader.h"


///// CHARACTER STRUCTURE /////
struct Character
{
	GLuint _textureID;
	glm::ivec2 _size;
	glm::ivec2 _bearing;
	GLuint _advance;
};


///// TEXT CLASS /////
class Text
{
public:
	Text();
	~Text();

	bool initialise();
	bool loadFont(const std::string& fileName, const GLuint fontHeight);
	void renderText(const std::string& text, GLfloat x, GLfloat y, const GLfloat scale, const glm::vec3 colour);

protected:
	FT_Library _freeType;
	FT_Face _face;
	std::map<GLchar, Character> _characters;		// Stores generated glyphs
	Shader* _shader;
	glm::mat4x4 _projection;

	GLuint _vbo;
	GLuint _vao;
};

#endif // TEXT_H