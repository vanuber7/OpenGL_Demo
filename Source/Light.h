#ifndef LIGHT_H
#define LIGHT_H

#include "GLAD/glad.h"
#include "glm/gtc/type_ptr.hpp"
#include "Shader.h"

///// LIGHT CLASS (ABSTRACT) /////
class Light
{
public:
	virtual ~Light();

	virtual bool initialise() = 0;

	void setMatrix4x4(const GLchar * name, const glm::mat4& value);
	void setVector3(const GLchar * name, const glm::vec3& value);
	void setFloat(const GLchar * name, const GLfloat& value);
	void setInt(const GLchar * name, const GLint& value);
	void setSampler(const GLchar * name, const GLint& textureSlot);

	void useShader();
	std::string getType() const;

protected:
	Light();

	Shader* _lightingShader;
	std::string _type;
};

#endif // LIGHT_H