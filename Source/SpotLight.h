#ifndef SPOT_LIGHT_H
#define SPOT_LIGHT_H

#include "Light.h"

///// SPOT LIGHT CLASS /////
class SpotLight : public Light
{
public:
	SpotLight();
	~SpotLight();

	// Inherited via Light
	virtual bool initialise() override;
};

#endif // SPOT_LIGHT_H

