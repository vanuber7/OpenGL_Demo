#include "Light.h"

///// DEFAULT CONSTRUCTOR /////
Light::Light()
{
	_lightingShader = new Shader();
}


///// DESTRUCTOR /////
Light::~Light()
{
	if (_lightingShader != nullptr)
	{
		delete _lightingShader;
		_lightingShader = nullptr;
	}
}


///// SET MATRIX 4X4 /////
void Light::setMatrix4x4(const GLchar * name, const glm::mat4 & value)
{
	_lightingShader->setUniform(name, value);
}


///// SET VECTOR3 /////
void Light::setVector3(const GLchar * name, const glm::vec3 & value)
{
	_lightingShader->setUniform(name, value);
}


///// SET FLOAT /////
void Light::setFloat(const GLchar * name, const GLfloat & value)
{
	_lightingShader->setUniform(name, value);
}


///// SET INT /////
void Light::setInt(const GLchar * name, const GLint & value)
{
	_lightingShader->setUniform(name, value);
}


///// SET SAMPLER /////
void Light::setSampler(const GLchar * name, const GLint & textureSlot)
{
	_lightingShader->setUniformSampler(name, textureSlot);
}


///// USE SHADER /////
void Light::useShader()
{
	if (_lightingShader != nullptr)
	{
		_lightingShader->use();
	}
}


///// GET TYPE /////
std::string Light::getType() const
{
	return _type;
}
