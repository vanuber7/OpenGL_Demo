#include <iostream>
#include "Window.h"
#include "Constants.h"

///// DEFAULT CONSTRUCTOR /////
Window::Window():
	_glfwWindow(nullptr),
	_GAME_TITLE("OpenGL Demo"),
	_FULL_SCREEN(GL_TRUE)
{
}


///// DESTRUCTOR /////
Window::~Window()
{
	glfwTerminate();
}


///// CREATE WINDOW /////
bool Window::createWindow()
{
	// We must initialise GLFW before using any OpenGL functions
	if (!glfwInit())
	{
		std::cerr << "GLFW failed to initialise" << std::endl;
		return false;
	}

	// Version 3.3 - Core profile
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Create the _window
	if (_FULL_SCREEN)
	{
		// Get monitor capabilities
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* videoMode = glfwGetVideoMode(monitor);
		if (videoMode != nullptr)
		{
			_glfwWindow = glfwCreateWindow(videoMode->width, videoMode->height, _GAME_TITLE, monitor, nullptr);
		}
	}
	else
	{
		_glfwWindow = glfwCreateWindow(screenWidth, screenHeight, _GAME_TITLE, nullptr, nullptr);
	}


	if (_glfwWindow == nullptr)
	{
		std::cerr << "Failed to create window..." << std::endl;
		return false;
	}

	// Make the context of the window the main context on the current thread
	glfwMakeContextCurrent(_glfwWindow);

	// Disable V-Sync for FPS benchmarking
	glfwSwapInterval(0);

	// Disable, hide and position the cursor center screen
	glfwSetInputMode(_glfwWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(_glfwWindow, screenWidth / 2.0, screenHeight / 2.0);

	// Initialise GLAD which loads the function pointers for the specific OS
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cerr << "Failed to initialise GLAD" << std::endl;
		return false;
	}

	// Set the buffer clear colour & update viewport size
	glClearColor(0.0f, 0.5f, 0.9f, 1.0f);
	glViewport(0, 0, screenWidth, screenHeight);
	// Enable depth buffer (Near verts drawn first)
	glEnable(GL_DEPTH_TEST);

	// Everything was ok!
	return true;
}


///// GET WINDOW /////
GLFWwindow * Window::getHandle() const
{
	return _glfwWindow;
}
