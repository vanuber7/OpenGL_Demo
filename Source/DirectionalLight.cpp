#include "DirectionalLight.h"

///// DEFAULT CONSTRUCTOR /////
DirectionalLight::DirectionalLight()
{
	_type = "DirectionalLight";
	initialise();
}


///// DESTRUCTOR /////
DirectionalLight::~DirectionalLight()
{
	// Parent 'Light' deletes shader pointer
}


///// INITIALISE /////
bool DirectionalLight::initialise()
{
	if (_lightingShader->loadShaders("Shaders/LightingDirectional.vert", "Shaders/LightingDirectional.frag"))
	{
		return true;
	}
	return false;
}
