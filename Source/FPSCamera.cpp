#include "FPSCamera.h"
#include "glm/gtc/constants.hpp"


///// DEFAULT CONSTRUCTOR /////
FPSCamera::FPSCamera(glm::vec3 position)
{
	_position = position;
	_yaw = glm::pi<float>();					// -Z starting angle (Into screen)
}


///// DESTRUCTOR /////
FPSCamera::~FPSCamera()
{
}


///// ROTATE /////
void FPSCamera::rotate(float yaw, float pitch)
{
	_yaw += glm::radians(yaw);
	_pitch += glm::radians(pitch);

	// Limit pitching up and down to the sky and floor
	_pitch = glm::clamp(_pitch, (-glm::pi<float>() / 2.0f) + 0.2f, (glm::pi<float>() / 2.0f) - 0.2f);

	updateCameraVectors();
}


///// SET POSITION /////
void FPSCamera::setPosition(const glm::vec3 & position)
{
	_position = position;
}


///// MOVE /////
void FPSCamera::move(const glm::vec3& offsetPosition)
{
	_position += offsetPosition;
	updateCameraVectors();
}


///// UPDATE CAMERA VECTORS /////
void FPSCamera::updateCameraVectors()
{
	glm::vec3 newLook;
	// Spherical coordinates to Cartesian
	newLook.x = cosf(_pitch) * sinf(_yaw);
	newLook.y = sinf(_pitch);
	newLook.z = cosf(_pitch) * cosf(_yaw);
	_look = glm::normalize(newLook);

	// We use the our new 'look' direction with the world's up vector to
	// calculate the 'right' (X) axis direction. We then use this 'right'
	// with the 'look' to get the camera's new 'up' vector
	_right = glm::normalize(glm::cross(_look, WORLD_UP));
	_up = glm::normalize(glm::cross(_right, _look));

	_targetPosition = _position + _look;
}
