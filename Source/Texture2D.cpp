#include <iostream>
#include "Texture2D.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"

///// DEFAULT CONSTRUCTOR /////
Texture2D::Texture2D():
	_texture(0)
{
}


///// DESTRUCTOR /////
Texture2D::~Texture2D()
{
	glDeleteTextures(1, &_texture);
}


///// LOAD TEXTURE /////
bool Texture2D::loadTexture(const std::string& fileName, bool generateMipmaps)
{
	GLint width, height, channels;

	// We want to invert the image
	stbi_set_flip_vertically_on_load(true);

	unsigned char* imageData = stbi_load(fileName.c_str(), &width, &height, &channels, STBI_rgb_alpha);

	if (imageData == nullptr)
	{
		std::cerr << "Error loading texture " << fileName << std::endl;
		return false;
	}

	glGenTextures(1, &_texture);
	glBindTexture(GL_TEXTURE_2D, _texture);
	// Wrap UV coords
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Shrink/Stretch the image to fit the quad we are drawing
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);

	if (generateMipmaps)
	{
		// Create smaller versions of this texture for different LOD
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	// Free image memory and unbind the texture
	stbi_image_free(imageData);
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}


///// BIND /////
void Texture2D::bind(GLuint texUnit)
{
	// Set which texture is currently active (for multi-texturing)
	glActiveTexture(GL_TEXTURE0 + texUnit);
	glBindTexture(GL_TEXTURE_2D, _texture);
}


///// UNBIND /////
void Texture2D::unbind(GLuint texUnit)
{
	// Same as above but bind the texture to nothing
	glActiveTexture(GL_TEXTURE0 + texUnit);
	glBindTexture(GL_TEXTURE_2D, 0);
}
