#version 330 core

struct Material
{
	vec3 ambient;
	sampler2D diffuseMap;
	vec3 specular;
	float shininess;
};

struct SpotLight
{
	vec3 position;
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	// Attenuation falloff values
	float constant;		
	float linear;
	float exponent;

	// Angles in radians either side of the spot light beam
	float cosInnerCone;		
	float cosOuterCone;
	bool on;
};

in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPosition;

out vec4 fragColour;

uniform SpotLight light;
uniform Material material;
uniform vec3 eyePosition;


vec3 calcSpotLight();

void main()
{
	// Ambient
	vec3 ambient = light.ambient * material.ambient * vec3(texture(material.diffuseMap, TexCoord));
	
	vec3 spotColour = vec3(0.0f);

	// Only change the spot light colour if the light is on
	if(light.on)
	{	
		spotColour = calcSpotLight();
	}

	fragColour = vec4(ambient + spotColour, 1.0f);
};


vec3 calcSpotLight()
{
	vec3 lightDirection = normalize(light.position - FragPosition);				
	vec3 spotDirection = normalize(light.direction);

	// Negate the 'lightDirection' so it faces the same way as the 'spotDirection'
	float cosDirection = dot(-lightDirection, spotDirection);
	// Smoothly interpolate the light intensity between the cones
	float spotIntensity = smoothstep(light.cosOuterCone, light.cosInnerCone, cosDirection);


	// Diffuse
	vec3 normal = normalize(Normal);
	// Use 'dot' to find the angle (and intensity) of the light on this surface normal
	// Clamp result to not be negative
	float nDotL = max(dot(normal, lightDirection), 0.0f);
	// Texture is present in the material
	vec3 diffuse = light.diffuse * vec3(texture(material.diffuseMap, TexCoord)) * nDotL;


	// Specular (Blinn-Phong lighting model)
	vec3 eyeDirection = normalize(eyePosition - FragPosition);
	// Half vector is the resultant vector of the light and eye directions
	vec3 halfDirection = normalize(lightDirection + eyeDirection);
	// Scale factor is the angle between the surface normal and half vector
	float nDotH = max(dot(normal, halfDirection), 0.0f);
	vec3 specular = light.specular * material.specular * pow(nDotH, material.shininess);

	float distance = length(light.position - FragPosition);
	// Calculate the falloff of the light over distance
	float attenuation = 1.0f / (light.constant + (light.linear * distance) + (light.exponent * pow(distance, 2)));

	diffuse *= attenuation * spotIntensity;
	specular *= attenuation * spotIntensity;

	return (diffuse + specular);
}