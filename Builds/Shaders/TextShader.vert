#version 330 core

layout (location = 0) in vec4 vertex;		// Stores the xy position AND texture coords

out vec2 TexCoord;

uniform mat4 projection;

void main()
{
	gl_Position = projection * vec4(vertex.xy, 0.0f, 1.0f);		// Multiply by projection matrix to get to NDC (Screen coords)
	TexCoord = vertex.zw;
};