#version 330 core

in vec2 TexCoord;
out vec4 fragColour;

uniform sampler2D textSampler;
uniform vec3 textColour;

void main()
{
	vec4 sampledText = vec4(1.0f, 1.0f, 1.0f, texture(textSampler, TexCoord).r);
	fragColour = vec4(textColour, 1.0f) * sampledText;
};