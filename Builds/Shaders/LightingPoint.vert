#version 330 core

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;

out vec3 Normal;
out vec2 TexCoord;
out vec3 FragPosition;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	Normal = mat3(transpose(inverse(model))) * normal;			// In case we perform non uniform scaling, the normal will remain perpendicular
	TexCoord = texCoord;
	FragPosition = vec3(model * vec4(pos, 1.0f));				// Needs to be in world space for lighting calculations
	gl_Position = projection * view * model * vec4(pos, 1.0f);	
};