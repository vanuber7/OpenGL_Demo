#version 330 core

struct Material
{
	vec3 ambient;
	sampler2D diffuseMap;
	vec3 specular;
	float shininess;
};

struct PointLight
{
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float exponent;
};

in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPosition;

out vec4 fragColour;

uniform PointLight light;
uniform Material material;
uniform vec3 eyePosition;

void main()
{
	// Ambient
	vec3 ambient = light.ambient * material.ambient * vec3(texture(material.diffuseMap, TexCoord));


	// Diffuse
	vec3 normal = normalize(Normal);
	vec3 lightDirection = normalize(light.position - FragPosition);											// We need direction FROM fragment TO the light source
	// Use 'dot' to find the angle (and intensity) of the light on this surface normal
	// Clamp result to not be negative
	float nDotL = max(dot(normal, lightDirection), 0.0f);
	vec3 diffuse = light.diffuse * vec3(texture(material.diffuseMap, TexCoord)) * nDotL;		// Texture is present in the material


	// Specular (Blinn-Phong lighting model)
	vec3 eyeDirection = normalize(eyePosition - FragPosition);
	vec3 halfDirection = normalize(lightDirection + eyeDirection);							// Half vector is the resultant vector of the light and eye directions
	float nDotH = max(dot(normal, halfDirection), 0.0f);									// Scale factor is the angle between the surface normal and half vector
	vec3 specular = light.specular * material.specular * pow(nDotH, material.shininess);

	float distance = length(light.position - FragPosition);
	float attenuation = 1.0f / (light.constant + (light.linear * distance) + (light.exponent * pow(distance, 2)));		// The fallout of the light over distance

	diffuse *= attenuation;
	specular *= attenuation;

	fragColour = vec4(ambient + diffuse + specular, 1.0f);
};