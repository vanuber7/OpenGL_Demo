#version 330 core

struct Material
{
	vec3 ambient;
	sampler2D diffuseMap;
	vec3 specular;
	float shininess;
};

struct DirectionalLight
{
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPosition;

out vec4 fragColour;

uniform DirectionalLight light;
uniform Material material;
uniform vec3 eyePosition;

void main()
{
	// Ambient
	vec3 ambient = light.ambient * material.ambient;


	// Diffuse
	vec3 normal = normalize(Normal);
	vec3 lightDirection = normalize(-light.direction);											// We need direction FROM fragment TO the light source
	// Use 'dot' to find the angle (and intensity) of the light on this surface normal
	// Clamp result to not be negative
	float nDotL = max(dot(normal, lightDirection), 0.0f);
	vec3 diffuse = light.diffuse * vec3(texture(material.diffuseMap, TexCoord)) * nDotL;		// Texture is present in the material


	// Specular (Blinn-Phong lighting model)
	vec3 eyeDirection = normalize(eyePosition - FragPosition);
	vec3 halfDirection = normalize(lightDirection + eyeDirection);							// Half vector is the resultant vector of the light and eye directions
	float nDotH = max(dot(normal, halfDirection), 0.0f);									// Scale factor is the angle between the surface normal and half vector
	vec3 specular = light.specular * material.specular * pow(nDotH, material.shininess);

	fragColour = vec4(ambient + diffuse + specular, 1.0f);
};